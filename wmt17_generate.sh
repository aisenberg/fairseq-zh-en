#!/bin/sh

DATADIR=data-bin/wmt17_zh_en
TRAIN=trainings/wmt17_zh_en
OUTPUT=tmp/wmt17_zh_en/fconv_test

echo "optimizing fconv for decoding"
fairseq optimize-fconv -input_model $TRAIN/fconv/model_best.th7 -output_model $TRAIN/fconv/model_best_opt.th7

echo "decoding to ${OUTPUT}"
fairseq generate -path $TRAIN/fconv/model_best_opt.th7 -datadir $DATADIR \
     -beam 10 -nbest 2 -dataset test -sourcelang zh -targetlang en | tee $OUTPUT.tmp

# TODO: decode subword BPE
cat $OUTPUT.tmp | sed -r 's/(@@ )|(@@ ?$)//g' > $OUTPUT.out
